FROM node:10
WORKDIR /usr/src/app
COPY ./dist/package*.json ./dist/
RUN cd dist && npm i
COPY ./dist ./dist/
COPY ./config ./config/
CMD ["node", "./dist/server.js"]


