import React from 'react';
import './Router.css';
import {
  BrowserRouter as Router,
  Route,
  NavLink,
} from "react-router-dom";
import { Container} from 'react-bootstrap';
import { NavbarComponent } from '../Ui-lib/Navbar/Navbar.component';
import { ProductListWrapper } from "../ProductListWrapper/ProductListWrapper.component";
import { ProductPage as ProductComponent } from "../Product/Product.component";
import { Wishlist } from "../Wishlist/Wishlist.component";
import { AddProduct } from '../addProduct/AddProduct.component';
import { UpdateProduct } from "../updateProduct/UpdateProduct.component";

export function RouterComponent () {
return(
        <Router>
            <NavbarComponent>
                <NavLink to="/" className="nav-link link">All products</NavLink>
                <NavLink to="/addProduct" className="nav-link link">Add Product</NavLink>
                <NavLink to="/wishlist" className="nav-link link">Wishlist</NavLink> 
            </NavbarComponent>
            <Container fluid>
                <Route exact path="/" component={ProductListWrapper}/>
                <Route path="/product/:id" component={ProductComponent}/>                        
                <Route path="/addProduct" component={AddProduct}/>                        
                <Route path="/updateProduct/:id" component={UpdateProduct}/>                        
                <Route path="/wishlist" component={Wishlist}/>
            </Container>
            <Container fluid className="mt-3">
                <hr/>
                <p className="text-center text-secondary">Done with <span role="img" aria-label="heart">💖</span> by Marcos Orive Izarra. This webapp is coronavirus<span role="img" aria-label="heart">🦠</span> free.</p>
            </Container>
        </Router>
    );
}
