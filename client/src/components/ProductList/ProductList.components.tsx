import React, {FunctionComponent} from "react";
import { ProductCard } from "../ProductCard/ProductCard.component";
import { CardColumns } from "react-bootstrap";
import { Product } from "../../models/Product.model";

interface ListProps{
    products: Product[];
}

export const ProductList : FunctionComponent<ListProps> = (props: ListProps) =>{
    return <CardColumns>
                {props.products.map((p) =>{
                    return <ProductCard product={p} key={p.name}/>
                })}
            </CardColumns>
}