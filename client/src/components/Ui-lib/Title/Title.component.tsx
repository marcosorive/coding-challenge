import React, { FunctionComponent } from "react";

interface TitleProps{
    text: string;
    color?: string;
}

export const Title: FunctionComponent<TitleProps> = (TitleProps) => {
    let textColor: string;
    switch(TitleProps.color){
        case "red":
            textColor = "text-danger";
            break;
        default:
            textColor = "";
    }
    return <h1 className={"text-center mt-3" + textColor}>{TitleProps.text}</h1>
}