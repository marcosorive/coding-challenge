import React, { FunctionComponent } from 'react'
import { Navbar} from 'react-bootstrap';

interface NavbarProps{
    children: React.ReactNode;
}

export const NavbarComponent: FunctionComponent<NavbarProps> = (props: NavbarProps) => {
    return(
        <Navbar collapseOnSelect expand="lg" bg="primary" variant="dark" >
					<Navbar.Brand>Coding challenge</Navbar.Brand>
					<Navbar.Toggle aria-controls="responsive-navbar-nav" />
					<Navbar.Collapse id="responsive-navbar-nav">
                        {props.children}
					</Navbar.Collapse>
				</Navbar>
    )
}