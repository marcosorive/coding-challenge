import React, { FunctionComponent } from "react";
import { Title } from "../Title/Title.component";

export const ErrorMessage: FunctionComponent<{}> = () => {
    return  <Title text="An error occurred. Please try again later." color="red"/>
}