import React, {ComponentState, ReactChild} from 'react';
import { Product } from "../../models/Product.model";
import { Card, Form, Button, Spinner, Alert } from "react-bootstrap";
import { Redirect, match } from "react-router-dom";
import { ErrorMessage } from "../Ui-lib/ErrorMessage/ErrorMessage.component";
import { Title } from '../Ui-lib/Title/Title.component';
import { PRODUCT_ENDPOINT } from "../../utils/constants/ApiEndpoints";

type matchParams = {
    id: string
}

interface UpdateProps {
    match: match<matchParams>
}

interface updateState {
    productId:string;
    product?: Product;
    productNotFound: boolean;
    productUpdated: boolean;
    error: boolean;
    loading: boolean;
    formError: boolean;
    name: string;
    description: string;
    stock: string;
    category: string;
}

export class UpdateProduct extends React.Component<UpdateProps,updateState> {
    constructor(props: UpdateProps) {
        super(props);
        this.state = {
            productId: this.props.match.params.id,
            product: undefined,
            productNotFound: false,
            productUpdated: false,
            error: false,
            loading: false,
            formError: false,
            name: "",
            description: "",
            stock: "",
            category: "",
        }
        this.handleInputChange = this.handleInputChange.bind(this);
        this.validateForm = this.validateForm.bind(this);
        this.sendPutRequest = this.sendPutRequest.bind(this);
        this.submitForm = this.submitForm.bind(this);
        this.renderForm = this.renderForm.bind(this);
    }

    componentDidMount(){
        this.setState({
            loading:true
        })
        this.getProduct();
    }

    private async getProduct(){
        const url = PRODUCT_ENDPOINT.concat(this.state.productId);
        try {
            const response = await fetch(url);
            if(response.status === 200){
                const product: Product = await response.json()
                this.setState({
                    product: product,
                    name: product.name,
                    description: product.description,
                    category: product.category,
                    stock: product.stock,
                    loading: false           
                }
            )
            }else {
                this.setState({
                    productNotFound: true,
                    loading:false
                })
            }
        } catch (error) {
            this.setState({
                error: true,
                loading:false
            })
        }
    }

    private handleInputChange(event: any): void{
        this.setState({
            [event.target.name] : event.target.value
        } as ComponentState)
    }

    private validateForm(): boolean{
        const {name, description, stock, category} = this.state;
        let result;
        if( name !== "" &&
            description !== "" &&
            stock !== "" &&
            category !== ""
        ){
            this.setState({
                formError: false
            })
            result = true;
        }
        else{
            this.setState({
                formError: true
            })
            result = false;
        }
        return result;
    }



    private async sendPutRequest(): Promise<void>{
        this.setState({
            loading: true
        })
        const body = {
            name: this.state.name,
            description: this.state.description,
            stock: this.state.stock,
            category: this.state.category
        } 
        try {
            const url = PRODUCT_ENDPOINT.concat(this.state.productId);
            const response = await fetch(url,{
                method: "Put",
                headers: {
                    "accept": "application/json",
                    "Content-Type": "application/json"
                },
                body: JSON.stringify(body)
            });
            if(await response.ok){
                this.setState({
                    loading: false,
                    productUpdated: true
                })
            }
        } catch (error) {
            this.setState({
                loading: false,
                error: true,
            })
        }
    }

    private submitForm(): void{
        if(this.validateForm()){
            this.sendPutRequest();
        }
    }

    private renderForm(): ReactChild{
        return(
            <>
            <Card className="mt-3">
            <Card.Body>                
                {this.state.formError ? <Alert variant="danger">Please fill all fields</Alert> : <></> }
                <Form>
                    <Form.Group>
                        <Form.Label>Product Name</Form.Label>
                        <Form.Control type="text" placeholder="Product Name" onChange={this.handleInputChange} value={this.state.name} name="name"/>
                    </Form.Group>    
                    <Form.Group>                        
                        <Form.Label>Description</Form.Label>
                        <Form.Control as="textarea" rows="3" placeholder="Description" onChange={this.handleInputChange} value={this.state.description} name="description"/>
                    </Form.Group>
                    <Form.Group>                        
                        <Form.Label>Stock</Form.Label>
                        <Form.Control type="number" min="0" placeholder="Stock (numeric value only)" onChange={this.handleInputChange} value={this.state.stock} name="stock"/>
                    </Form.Group>
                    <Form.Group>                        
                        <Form.Label>Category</Form.Label>
                        <Form.Control as="select" onChange={this.handleInputChange} value={this.state.category} name="category">
                            <option value="" disabled>Select a category</option>
                            <option value="0">Shoes</option>
                            <option value="1">Shirts</option>
                            <option value="2">Pants</option>
                            <option value="3">Accesories</option>
                            <option value="4">Others</option>
                        </Form.Control>
                    </Form.Group>
                </Form>
                <Button variant="primary" onClick={this.submitForm}>Update</Button>
            </Card.Body>
            </Card>
            </>
        )
    }

    render(){
        const {error, loading, productUpdated, productId, productNotFound, product} = this.state;
        let content;
        if(error){
            content = <ErrorMessage/>
        }else if(loading){
            content =   <Spinner animation="border" role="status" variant="primary">
                            <span className="sr-only">Loading...</span>
                        </Spinner>
        }else if(productUpdated){
            content = <Redirect to={"/product/"+productId}/>
        }else if(product !== undefined ){
            content = this.renderForm();
        }
        else if(productNotFound){
            content = <Title text="Looks like that product is no longer available."/>
        }
        return(
            <>
                <Title text="Update product"/>
                {content}
            </>
        )
    }
}