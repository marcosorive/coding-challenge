import React from 'react';
import {match, Redirect, NavLink} from 'react-router-dom';
import { Product as ProductModel} from '../../models/Product.model';
import {Spinner, Card, Button, ButtonGroup} from 'react-bootstrap';
import { PRODUCT_ENDPOINT,WISHLIST_ENDPOINT } from "../../utils/constants/ApiEndpoints";
import { getCategoryString } from "../../utils/adapters/CategoryAdapter";
import { Title } from '../Ui-lib/Title/Title.component';
import { ErrorMessage } from '../Ui-lib/ErrorMessage/ErrorMessage.component';

type matchParams = {
    id: string
}

type ProductProps = {
    match: match<matchParams>
}

type ProductState = {
    product?: ProductModel;
    error: boolean;
    productNotfound: boolean;
    loading: boolean;
    productId: string;
    wishlistId: string;
    addedToWishlist: boolean;
    productDeleted: boolean;
}


export class ProductPage extends React.Component<ProductProps,ProductState> {

    constructor(props: ProductProps) {
        super(props);
        this.state = {
            error: false,
            loading: false,
            product: undefined,
            productNotfound: false,
            productId: this.props.match.params.id,
            wishlistId: localStorage.getItem("wishlistId") || "",
            addedToWishlist: false,
            productDeleted: false
        }
        this.getProduct = this.getProduct.bind(this);
        this.handleAddToWishlist = this.handleAddToWishlist.bind(this);
        this.addToWishlist = this.addToWishlist.bind(this);
        this.createWishlsit = this.createWishlsit.bind(this);
        this.deleteProduct = this.deleteProduct.bind(this);
    }

    componentDidMount(){
        this.setState({
            loading: true
        })
        this.getProduct();
    }

    private async getProduct(){
        const url = PRODUCT_ENDPOINT.concat(this.state.productId);
        try {
            const response = await fetch(url);
            if(response.status === 200){
                this.setState({
                    product: await response.json(),     
                    loading: false           
                }
            )
            }else {
                this.setState({
                    productNotfound: true,
                    loading:false
                })
            }
        } catch (error) {
            this.setState({
                error: true,
                loading:false
            })
        }
    }

    private handleAddToWishlist(): void{
        if(this.state.wishlistId !== ""){
            this.addToWishlist(this.state.wishlistId,this.state.productId);
        }else{
            this.createWishlsit(this.state.productId)
        }
    }

    private async createWishlsit(productId: string): Promise<void>{
        const body = {
            productId
        }
        try {
            const response = await fetch(WISHLIST_ENDPOINT,{
                method: "POST",
                headers : {
                    "accept": "application/json",
                    "Content-Type": "application/json"
                },
                body: JSON.stringify(body),
            })
            if(await response.ok){
                this.setState({
                    addedToWishlist:true,
                })
                const wishlist = await response.json();
                localStorage.setItem('wishlistId', await wishlist._id);
            }else{
                this.setState({
                    error: true,
                })
            }
        } catch (error) {
            this.setState({
                error: true,
            })
        }
    }

    private async addToWishlist(wishlistId: string,productId: string): Promise<void>{
        const body = {
            wishlistId,
            productId        
        }
        try {
            const url = WISHLIST_ENDPOINT.concat("product");
            const response = await fetch(url,{
                method: "POST",
                headers : {
                    "accept": "application/json",
                    "Content-Type": "application/json"
                },
                body: JSON.stringify(body),
            })
            if(await response.ok){
                this.setState({
                    addedToWishlist:true,
                })
            }else{
                this.setState({
                    error: true,
                })
            }
        } catch (error) {
            this.setState({
                error: true,
            })
        }
    }

    private async deleteProduct(): Promise<void>{
        const url = PRODUCT_ENDPOINT.concat(this.state.productId);
        try {
            const response = await fetch(url,{
                method: "DELETE"
            });
            if(response.status === 200){
                this.setState({
                    productDeleted: true,
                    loading: false           
                }
            )
            }else {
                this.setState({
                    productNotfound: false,
                    error:true,
                    loading:false
                })
            }
        } catch (error) {
            this.setState({
                error: true,
                loading:false
            })
        }
    }

    render(){
        const {product, error, loading, productNotfound,addedToWishlist, productDeleted} = this.state;
        let content;
        if(loading){
            content =   <Spinner animation="border" role="status">
                            <span className="sr-only">Loading...</span>
                        </Spinner>
        }
        else if(productNotfound){
            content = <Title text="Looks like that product is no longer available."/>
        }
        else if(error){
            content = <ErrorMessage/>
        }
        else if(productDeleted){
            content = <Redirect to="/"/>
        
        }
        else if (product !== undefined){
            content =
                <div className="container-fluid">
                    <Card>
                        <Card.Header as="h1">{product.name}</Card.Header>
                        <Card.Body>
                            <Card.Title>Stock: {product.stock}</Card.Title>
                            <Card.Title>Category: {getCategoryString(parseInt(product.category))}</Card.Title>
                            <Card.Text>
                                {product.description}
                            </Card.Text>
                            <ButtonGroup>
                                <NavLink to={"/updateProduct/"+this.state.productId}><Button variant="primary">Modify</Button></NavLink>
                                {   addedToWishlist ? 
                                    <Button variant="secondary">Addded</Button> :                                    
                                    <Button variant="secondary" onClick={this.handleAddToWishlist}>Add to wishlist</Button>
                                }
                                <Button variant="danger" onClick={this.deleteProduct}>Delete</Button>
                            </ButtonGroup>
                        </Card.Body>
                        </Card>
                </div>
        }
        return(
            <>
                <Title text="Product"/>
                {content}
            </>
        )
    }
}