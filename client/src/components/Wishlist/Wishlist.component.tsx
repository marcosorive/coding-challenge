import React from 'react';
import { Title } from '../Ui-lib/Title/Title.component';
import { ErrorMessage } from '../Ui-lib/ErrorMessage/ErrorMessage.component';
import { Product } from "../../models/Product.model";
import { CardColumns, Spinner } from "react-bootstrap";
import { ProductCard } from "../ProductCard/ProductCard.component";
import { WISHLIST_ENDPOINT } from "../../utils/constants/ApiEndpoints";
interface WishlistListProps {

}

interface WishlistListState {
    wishlistId: string
    error: boolean,
    products: Product[],
    loading: boolean,
}

export class Wishlist extends React.Component<WishlistListProps,WishlistListState> {
    constructor(props: WishlistListProps) {
        super(props);
        this.state = {
            wishlistId: localStorage.getItem('wishlistId') || "",
            error: false,
            products: [],
            loading: false,
        }
        this.deleteFromArray = this.deleteFromArray.bind(this);
    }
    
    componentDidMount(){
        this.setState({
            loading:true
        })
        this.getWishListProducts();
    }

    private async getWishListProducts(): Promise<void>{
        const {wishlistId} = this.state;
        if(wishlistId === ""){
            this.setState({
                loading: false
            })
            return;
        }
        try {
            const url = WISHLIST_ENDPOINT.concat(this.state.wishlistId+"/products")
            const reponse = await fetch(url);
            if(await reponse.ok){
                this.setState({
                    products: await reponse.json(),
                    loading: false,
                })
            }else{
                this.setState({
                    error:true,
                    loading:false
                })
            }
        } catch (error) {
            this.setState({
                error:true,
                loading: false
            })
        }
    }

    private deleteFromArray(productId: string){
        const newProducts = this.state.products.filter((p) => {
            return p._id !== productId;
        });
        this.setState({
            products: newProducts,
        })
    }

    render(){
        const {wishlistId, products, loading, error} = this.state;
        let content;
        if(loading){
            content =   <Spinner animation="border" role="status">
                            <span className="sr-only">Loading...</span>
                        </Spinner>
        }
        else if(wishlistId === "" || products.length === 0){
            content = <Title text="There's nothing in your wishlist. Go add something!"/>
        }
        else if(error){
            content = <ErrorMessage/>
        }
        else{
            content = 
                <CardColumns>
                    {products.map((p) =>{
                        if(p === null){
                            return (<></>);
                        }else{
                            return <ProductCard product={p} key={p._id} inWishlist={true} deleteFromArray={this.deleteFromArray}/>
                        }                        
                    })}
                </CardColumns>         
        }
        return(
            <>
                <Title text="Your wishlist"/>
                {content}
            </>
        )
    }
}