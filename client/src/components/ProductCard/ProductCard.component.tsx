import React, { ReactChild } from 'react'
import { Card, Button, ButtonGroup} from 'react-bootstrap';
import { Redirect } from 'react-router-dom';
import { Title } from '../Ui-lib/Title/Title.component';
import { Product } from "../../models/Product.model";
import { WISHLIST_ENDPOINT } from "../../utils/constants/ApiEndpoints";
import {getCategoryString} from "../../utils/adapters/CategoryAdapter";
import './ProductCard.style.css'

interface ProductCardProps{
    product: Product;
    inWishlist?: boolean;
    deleteFromArray?: Function;
}

interface ProductCardState{
    redirectProductPage: boolean;
    product: Product;
    error: boolean;
    wishlistId: string;
}

export class ProductCard extends React.Component<ProductCardProps,ProductCardState> {
    
    constructor(props: ProductCardProps){
        super(props);
        this.state = {
            product: props.product,
            redirectProductPage: false,
            error: false,
            wishlistId: localStorage.getItem('wishlistId') || "",
        }
        this.redirect = this.redirect.bind(this);
        this.deleteFromWishlist = this.deleteFromWishlist.bind(this);
    }

    private redirect(): void{
        this.setState({
            redirectProductPage: true
        })
    }

    private async deleteFromWishlist(): Promise<void>{
        const body = {
            wishlistId: this.state.wishlistId,
            productId: this.state.product._id        
        }
        try {
            const url = WISHLIST_ENDPOINT.concat("product");
            const response = await fetch(url,{
                method: "delete",
                headers : {
                    "accept": "application/json",
                    "Content-Type": "application/json"
                },
                body: JSON.stringify(body),
            })
            if(await response.ok){
                if(this.props.deleteFromArray){
                    this.props.deleteFromArray(this.state.product._id);
                }
            }else{
                this.setState({
                    error: true
                })
            }
        } catch (error) {
            this.setState({
                error: true
            })
        }
    }

    render(){
        const {product, redirectProductPage, error } = this.state
        let content: ReactChild;
        if(error){
           content = <Title text="An error ocurred. Please try again later."/> 
        }
        else if(redirectProductPage){
            content = <Redirect to={'/product/'+product._id} />
        }
        else{
            let wishlistButton;
            if(this.props.inWishlist){
                wishlistButton = <Button variant="secondary" onClick={() => {this.deleteFromWishlist()}}>Delete from wishlist</Button>                
            }else{
                wishlistButton = <></>
            }
            content = (
                <Card>
                    <Card.Body>
                        <Card.Title>{product.name}</Card.Title>
                        <Card.Subtitle className="mb-2 text-muted">Stock: {product.stock}</Card.Subtitle>
                        <Card.Subtitle>Category: {getCategoryString(parseInt(product.category))}</Card.Subtitle>
                        <Card.Text className="mt-2">{product.description}</Card.Text>
                        <ButtonGroup className="container-fluid">
                            {wishlistButton}
                            <Button variant="primary" onClick={this.redirect}>See Product</Button>
                        </ButtonGroup>
                    </Card.Body>
                    <Card.Footer>
                        <small className="text-muted">Added: {product.dateAdded}</small>
                    </Card.Footer>
                </Card>
            )
        }
        return(
            <>{content}</>
        )
    }
}