import React, { ComponentState } from 'react';
import { Product } from "../../models/Product.model";
import { Title } from '../Ui-lib/Title/Title.component';
import { ErrorMessage } from "../Ui-lib/ErrorMessage/ErrorMessage.component";
import { Spinner, Form, Row, Col, Container} from "react-bootstrap";
import { ProductList } from "../ProductList/ProductList.components";
import { PRODUCT_ENDPOINT } from "../../utils/constants/ApiEndpoints";

interface WrapperProps {

}

interface WrapperState {
    allProducts: Product[];
    filteredProducts: Product[];
    error: boolean;
    loaded: boolean;
    productName: string;
    productCategory: string;
}

export class ProductListWrapper extends React.Component<WrapperProps,WrapperState> {
    constructor(props: WrapperProps) {
        super(props);
        this.state = {
            allProducts: [],
            filteredProducts: [],
            error: false,
            loaded: false,
            productName: "",
            productCategory: ""
        }
        this.getAllProducts = this.getAllProducts.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
    }   

    componentDidMount(){
        this.getAllProducts();
    }

    private async getAllProducts(): Promise<void>{
        const response = await fetch(PRODUCT_ENDPOINT);
        if(response.ok){
            const products: Product[] = await response.json();
            this.setState({
                allProducts: products,     
                filteredProducts: products,
                loaded: true           
            })
        }else{
            this.setState({
                error: true,
            })
        }
    }

    private handleInputChange(event: any):void{
        const inputChanged:string = event.target.name;
        const valueChanged:string = event.target.value;

        this.setState({
            [inputChanged]: valueChanged, 
        } as ComponentState)

        let filtered: Product[] = this.state.allProducts;
        console.log(valueChanged);
        filtered = filtered.filter((p) => {
            let result;
            if(inputChanged === "productName"){
                result = p.name.toLowerCase().includes(valueChanged.toLowerCase());
            }else if (inputChanged === "productCategory"){
                if(valueChanged === "-1"){
                    result = true;
                }else{
                    result = String(p.category) === valueChanged;
                }
            }
            return result;
        })
        this.setState({
            filteredProducts: filtered
        })
    }    

    render(){
        const {filteredProducts, error, loaded} = this.state;
        let content;
        if(!loaded){
            content =   <Spinner animation="border" role="status">
                            <span className="sr-only">Loading...</span>
                        </Spinner>
        }
        else if(error){
            content = <ErrorMessage />
        }else if(filteredProducts.length === 0){
            content = <Title text="Looks like there are no products. Go add one!"/>
        }else{
            content = <ProductList products={this.state.filteredProducts}/>   
        }
        return(
            <>
                <Title text="All products"/>
                <Container fluid className="border my-3 p-3">
                    <h3 className="text-center">Search products</h3>
                    <Form>
                        <Row>
                            <Col>
                                <Form.Group>
                                    <Form.Label>Product name</Form.Label>
                                    <Form.Control type="text" onChange={this.handleInputChange} value={this.state.productName} name="productName" placeholder="Product name. E.g.: Red pants, Sneakers..." />
                                </Form.Group>
                            </Col>
                            <Col>
                                <Form.Group>
                                    <Form.Label>Product category</Form.Label>
                                    <Form.Control as="select" name="productCategory" value={this.state.productCategory} onChange={this.handleInputChange}>
                                        <option disabled value="">Select a category</option>
                                        <option value="-1">All</option>
                                        <option value="0">Shoes</option>
                                        <option value="1">Shirts</option>
                                        <option value="2">Pants</option>
                                        <option value="3">Accesories</option>
                                        <option value="4">Others</option>
                                    </Form.Control>
                                </Form.Group>
                            </Col>
                        </Row>
                    </Form>
                </Container>
                <div>
                    {content}
                </div>
            </>
        )
    }
}