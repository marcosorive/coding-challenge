import React, {ComponentState, ReactChild} from 'react';
import { Product } from "../../models/Product.model";
import { Card, Form, Button, Spinner, Alert } from "react-bootstrap";
import { Redirect } from "react-router-dom";
import { Title } from '../Ui-lib/Title/Title.component';
import { PRODUCT_ENDPOINT } from "../../utils/constants/ApiEndpoints";

interface AddProductProps {

}

interface AddProductState {
    error: boolean;
    loading: boolean;
    formError: boolean;
    productAdded?: Product;
    name: string;
    description: string;
    stock: string;
    category: string;
}

export class AddProduct extends React.Component<AddProductProps,AddProductState> {
    constructor(props: AddProductProps) {
        super(props);
        this.state = {
            error: false,
            loading: false,
            formError: false,
            productAdded: undefined,
            name: "",
            description: "",
            stock: "",
            category: "",
        }
        this.handleInputChange = this.handleInputChange.bind(this);
        this.validateForm = this.validateForm.bind(this);
        this.sendPostRequest = this.sendPostRequest.bind(this);
        this.submitForm = this.submitForm.bind(this);
        this.renderForm = this.renderForm.bind(this);
    }

    private handleInputChange(event: any): void{
        this.setState({
            [event.target.name] : event.target.value
        } as ComponentState)
    }

    private validateForm(): boolean{
        const {name, description, stock, category} = this.state;
        let result;
        if( name !== "" &&
            description !== "" &&
            stock !== "" &&
            category !== ""
        ){
            this.setState({
                formError: false
            })
            result = true;
        }
        else{
            this.setState({
                formError: true
            })
            result = false;
        }
        return result;
    }

    private async sendPostRequest(): Promise<void>{
        this.setState({
            loading: true
        })
        const body = {
            name: this.state.name,
            description: this.state.description,
            stock: String(this.state.stock),
            category: this.state.category
        } 
        try {
            const response = await fetch(PRODUCT_ENDPOINT,{
                method: "POST",
                headers: {
                    "accept": "application/json",
                    "Content-Type": "application/json"
                },
                body: JSON.stringify(body)
            });
            if(await response.ok){
                this.setState({
                    loading: false,
                    productAdded: await response.json() as Product
                })
            }
        } catch (error) {
            this.setState({
                loading: false,
                error: true,
            })
        }
    }

    private submitForm(): void{
        if(this.validateForm()){
            this.sendPostRequest();
        }
    }

    private renderForm(): ReactChild{
        return(
            <>
            <Card className="mt-3">
            <Card.Body>                
                {this.state.formError ? <Alert variant="danger">Please fill all fields</Alert> : <></> }
                <Form>
                    <Form.Group>
                        <Form.Label>Product Name</Form.Label>
                        <Form.Control type="text" placeholder="Product Name" onChange={this.handleInputChange} value={this.state.name} name="name"/>
                    </Form.Group>    
                    <Form.Group>                        
                        <Form.Label>Description</Form.Label>
                        <Form.Control as="textarea" rows="3" placeholder="Description" onChange={this.handleInputChange} value={this.state.description} name="description"/>
                    </Form.Group>
                    <Form.Group>                        
                        <Form.Label>Stock</Form.Label>
                        <Form.Control type="number" min="0" placeholder="Stock (numeric value only)" onChange={this.handleInputChange} value={this.state.stock} name="stock"/>
                    </Form.Group>
                    <Form.Group>                        
                        <Form.Label>Category</Form.Label>
                        <Form.Control as="select" onChange={this.handleInputChange} value={this.state.category} name="category">
                            <option value="" disabled>Select a category</option>
                            <option value="0">Shoes</option>
                            <option value="1">Shirts</option>
                            <option value="3">Pants</option>
                            <option value="4">Accesories</option>
                            <option value="5">Others</option>
                        </Form.Control>
                    </Form.Group>
                </Form>
                <Button variant="primary" onClick={this.submitForm}>Add</Button>
            </Card.Body>
            </Card>
            </>
        )
    }

    render(){
        const {error, loading, productAdded} = this.state;
        let content;
        if(error){
            content = <p>An error ocurred, please try again later.</p>
        }else if(loading){
            content =   <Spinner animation="border" role="status" variant="primary">
                            <span className="sr-only">Loading...</span>
                        </Spinner>
        }else if(productAdded !== undefined){
            content = <Redirect to={"/product/"+productAdded._id}/>
        }else{
            content = this.renderForm();
        }
        return(
            <>
                <Title text="Add a product"/>
                {content}
            </>
        )
    }


}