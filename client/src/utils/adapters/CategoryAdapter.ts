import { Category } from "../../enums/Category";

export function getCategoryString(cat: number): string{
    return Category[cat] ? String(Category[cat]) : "Unknown category";
}