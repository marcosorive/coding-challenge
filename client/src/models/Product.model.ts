export class Product{
    constructor(
        public _id: string = _id,
        public name: string = name,
        public slug: string = slug,
        public description: string = description,
        public dateAdded: Date = dateAdded,
        public stock: string = stock,
        public category: string = category,
        ){}
}
