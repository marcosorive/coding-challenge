# Adidas Code Challenge

## By Marcos Orive Izarra

You can try the web app in: [http://adidas-coding-challenge.herokuapp.com](http://adidas-code-challenge.herokuapp.com)

## Summary
### Backend

- As stated in the provided document I've used NodeJS fo the backend. 
- I decided to use Express.js as a web framework on top of Node, mainly because it's simple and light. For the DBMS I've chosen MongoDB and Mongoose as ORM to work with it. 
- I've chosen Mongo for the quick start and flexibility it provides. The lack of schemas, tables, etc, makes the development process much faster. Having only 'Products' and 'Wishlist' wouldn't have been a difficult job thinking a relational schema. But MongoDB makes these things easier. 
- Mongoose helps to manage the database queries as objects, this is just a personal choice.
- Using Typescript is also a personal choice. I think that a strongly-typed language can save us from a lot of silly mistakes. I also like the Object-oriented paradigm that it provides.
- As a logger, I've chosen Winston. It's a popular logging tool for node.js.

### Frontend

- For the frontend, I've used "React". I created the app with create-react-app and the Typescript template it provides.
- I used typescript for the same reasons as before.
- At first, I thought I wouldn't need Redux. I'm not going to lie: I've barely used redux and I overestimated how useful it is. I could (and should) have used it to store the list of all the products and the state and products of the wishlist. 
- To speed things up I used react-bootstrap. A collection of React components that wrap bootstrap so it's easy to use. I usually like to write my own CSS so I can control it, but this time turned out nice.
- React-router: of course to manage routes inside react.

## Requirements to run:

- Node installed in your system.
- MongoDB installed in your system.
- Setting an environmental variable with the database URL called DB_URL. (For a local MongoDB: mongodb://localhost:27017/coding-challenge)
- Alternatively, you can modify the file /server/config/keys.js and set the Database URL there.

### For dev:

- Run `npm install` in both client and server directories. 
- Run `npm run web` in the root the project.

The server will listen in port 5000 unless you set an environmental variable called PORT.
The react development server will listen to port 3000. React is configured to redirect requests to port 5000.

### For production:

- You can build production code running the `build.sh` script in a Unix-like system. 
- Said script will create and populate a `dist` folder in the root of the project. 
- Please run `npm install` and `node server.js` to run the code.
- Again: The server will listen in port 5000 unless you set an environmental variable called PORT.

### Docker:

- To build a docker image, first, you need to run the `build.sh` script.
- You can build a docker image running "docker build . " use the -t flag to assign a name/tag to the image.
  - `docker build . -t adidas:code-challenge`
- After that, you can run the image using the associated tag. 
- As usual, the server will listen to port 5000. You can change that the flag: `-a PORT=80`
- If you want to link a port in your local machine to the container use the `-p flag: -p 8080:80`
- Finally, don\`t forget to the DB URL: `-e DB_URL=databaseurl`
  - `docker run -p 8080:80 -e PORT=80 -e DB_URL=databaseurl adidas:code-challenge`

### CI/CD

- I've built a little pipeline in Gitlab. 
- In the past I've successfully managed to deploy to Heroku with Gitlab, using a Ruby gem called 'dpl'. Deploying a Docker container wasn't so simple. I've tried what the documentation says, but it won't work.
- I've been able to deploy it locally using the `deploy.sh` script (after login in Heroku). Please feel free to use it if you want.
- Right now

### Testing

- I feel ashamed to say this but... I couldn't find time to write tests for the app. I could blame the current COVID-19 situation, but I think I just lost a lot of time with little details instead of focusing on the important.