import * as mongoose from 'mongoose';
import { IProduct } from '../interfaces/IProduct';
export interface IWishlist extends mongoose.Document {
    products: IProduct['_id'][];
};