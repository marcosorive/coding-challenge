import * as mongoose from 'mongoose';
import { IWishlist } from '../interfaces/IWishlist'

const wishlistSchema = new mongoose.Schema({
    products: {type: [mongoose.Schema.Types.ObjectId], default: []} 
});

export const Wishlist = mongoose.model<IWishlist>("Wishlist", wishlistSchema);