import { Router } from "express";
import { WishlistController } from "../controllers/WishlistController";

export function getWishlistRoutes(): Router{
    const router: Router = Router();
    router.get('/', WishlistController.getAllWishlist);
    router.post('/', WishlistController.createWishlist);
    router.get('/:id', WishlistController.getWishlist);
    router.delete('/product', WishlistController.deleteProductFromWishlist);
    router.delete('/:id', WishlistController.deleteWishlist);
    router.get('/:id/products', WishlistController.getAllProductsFromWishlist);    
    router.post('/product', WishlistController.addProductToWishlist);

    return router;
}