import { Router } from "express";
import { ProductController } from "../controllers/ProductController";

export function getProductRoutes(): Router{
    const router: Router = Router();
    router.get('', ProductController.getAllProducts);
    router.post('', ProductController.addProduct);
    router.get('/filter/:category', ProductController.getProductsByCategory);
    router.get('/:id', ProductController.getProduct);
    router.put('/:id', ProductController.updateProduct);
    router.delete('/:id', ProductController.deleteProduct);
    return router;
}