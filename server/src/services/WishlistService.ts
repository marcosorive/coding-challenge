import { IWishlistRepository } from '../repositories/wishlist/IWishlistRepository';
import { IWishlist } from 'interfaces/IWishlist';
import { Bootstrap } from '../Bootstrap';
import { Wishlist } from '../models/Wishlist';
import { IProduct } from 'interfaces/IProduct';
import { IProductRepository } from '../repositories/product/IProductRepository';

export class WishlistService{

    constructor(private wishlistRepository: IWishlistRepository,
                private productRepository: IProductRepository){}

    public async createWishlist(productId: string): Promise<IWishlist>{
        try {
            const wishlist = new Wishlist();
            wishlist.products.push(productId);
            return await this.wishlistRepository.saveOrCreateWishlist(wishlist);
        } catch (error) {
            Bootstrap.logger.log("error","Error in WishlistService.createWishlist: " + error);
            throw new Error(error);
        }
    }

    public async getAllWishlist(): Promise<IWishlist[]>{
        try {
            return await this.wishlistRepository.getAllWishlist();
        } catch (error) {
            Bootstrap.logger.log("error","Error in WishlistService.getAllWishlist: " + error);
            throw new Error(error);
        }
    }

    public async getWishlist(id: string): Promise<IWishlist>{
        try {
            return await this.wishlistRepository.getWishlistById(id)
        } catch (error) {
            Bootstrap.logger.log("error","Error in WishlistService.getWishlist: " + error);
            throw new Error(error);
        }
    }

    public async deleteWishlist(id: string): Promise<void>{
        try {
            return await this.wishlistRepository.deleteWishlistById(id)
        } catch (error) {
            Bootstrap.logger.log("error","Error in WishlistService.getWishlist: " + error);
            throw new Error(error);
        }
    }    

    public async getAllProductsFromWishlist(id: string): Promise<IProduct[]>{
        try {
            const wishlist: IWishlist = await this.wishlistRepository.getWishlistById(id)
            const products: Promise<IProduct[]> = Promise.all(wishlist.products.map(async (productId)=>{
                return await this.productRepository.getProduct(productId);
            }));
            const realProducts = (await products).filter((p) => {
                return p !== null;
            });
            return realProducts;
        } catch (error) {
            Bootstrap.logger.log("error","Error in WishlistService.getAllProductsFromWishlist: " + error);
            throw new Error(error);
        }
    }

    public async addProductToWishlist(wishlistId: string, productId: string): Promise<IWishlist>{
        try {
            const wishlist = await this.wishlistRepository.getWishlistById(wishlistId);
            const product = await this.productRepository.getProduct(productId);
            if(! wishlist.products.includes(await product._id)){
                wishlist.products.push(await product._id);            
                return await this.wishlistRepository.saveOrCreateWishlist(wishlist);
            }else{
                return wishlist;
            }
        } catch (error) {
            Bootstrap.logger.log("error","Error in WishlistService.addProductToWishlist: " + error);
            throw new Error(error);
        }
    }

    public async deleteProductFromWishlist(wishlistId: string, productId: string): Promise<IWishlist>{
        try {
            const wishlist = await this.wishlistRepository.getWishlistById(wishlistId);
            const product = await this.productRepository.getProduct(productId);
            wishlist.products = this.removeProductFromWishlistArray(wishlist.products, product._id);
            return await this.wishlistRepository.saveOrCreateWishlist(wishlist);
        } catch (error) {
            Bootstrap.logger.log("error","Error in WishlistService.deleteProductFromWishlist: " + error);
            throw new Error(error);
        }
    }

    private removeProductFromWishlistArray(productIdArray: string[], id: string): string[]{ 
        let array: string[] = productIdArray;
        const index = array.indexOf(id);
        if (index > -1) {
            array.splice(index, 1);
        }
        return array;
    }
}