import { Request, Response } from "express";
import {IWishlist} from '../interfaces/IWishlist';
import { Bootstrap } from '../Bootstrap';
import { IProduct } from "interfaces/IProduct";

export class WishlistController {

    public static async getAllWishlist(req: Request, res: Response): Promise<void>{
        try{
            const wishlist: IWishlist[] = await Bootstrap.wishlistService.getAllWishlist();
            res.status(200).send(wishlist);
        }
        catch(error){
            Bootstrap.logger.log("error","Error in  WishlistController.getAllWishlist: " + error);
            res.status(500).send({error: error})
        }
    }
    
    public static async createWishlist(req: Request, res: Response): Promise<void>{
        try{
            const wishlist: IWishlist = await Bootstrap.wishlistService.createWishlist(req.body.productId);
            res.status(200).send(wishlist);
        }
        catch(error){
            Bootstrap.logger.log("error","Error in  WishlistController.createWishlist: " + error);
            res.status(500).send({error: error})
        }
    }

    public static async getWishlist(req: Request, res: Response): Promise<void>{
        try{
            const wishlist: IWishlist = await Bootstrap.wishlistService.getWishlist(req.params.id);
            res.status(200).send(wishlist);
        }
        catch(error){
            Bootstrap.logger.log("error","Error in  WishlistController.getWishlist: " + error);
            res.status(500).send({error:  error})
        }
    }

    public static async deleteWishlist(req: Request, res: Response): Promise<void>{
        try{
            await Bootstrap.wishlistService.deleteWishlist(req.params.id);
            res.status(200).send();
        }
        catch(error){
            Bootstrap.logger.log("error","Error in  WishlistController.deleteWishlist: " + error);
            res.status(500).send({error:  error})
        }
    }    

    public static async getAllProductsFromWishlist(req: Request, res: Response): Promise<void>{
        try{
            const products: IProduct[] = await Bootstrap.wishlistService.getAllProductsFromWishlist(req.params.id);
            res.status(200).send(products);
        }
        catch(error){
            Bootstrap.logger.log("error","Error in  WishlistController.getAllProductsFromWishlist: " + error);
            res.status(500).send({error: error})
        }
    }

    public static async addProductToWishlist(req: Request, res: Response): Promise<void>{
        try{
            const products: IWishlist = await Bootstrap.wishlistService.addProductToWishlist(req.body.wishlistId, req.body.productId);
            res.status(200).send(products);
        }
        catch(error){
            Bootstrap.logger.log("error","Error in  WishlistController.addProductToWishlist: " + error);
            res.status(500).send({error: error})
        }
    }
    public static async deleteProductFromWishlist(req: Request, res: Response): Promise<void>{
        try{
            const products: IWishlist = await Bootstrap.wishlistService.deleteProductFromWishlist(req.body.wishlistId, req.body.productId);
            res.status(200).send(products);
        }
        catch(error){
            Bootstrap.logger.log("error","Error in  WishlistController.deleteProductFromWishlist: " + error);
            res.status(500).send({error: error})
        }
    }
}