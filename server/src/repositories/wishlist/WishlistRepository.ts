import {IWishlistRepository} from './IWishlistRepository';
import { IWishlist } from "../../interfaces/IWishlist";
import { IProduct } from "../../interfaces/IProduct";
import { Bootstrap } from "../../Bootstrap";
import { Wishlist } from '../../models/Wishlist';

export class WishlistRepository implements IWishlistRepository {
    constructor(private Wishlist){}

    public async getAllWishlist(): Promise<IWishlist[]> {
        try {            
            return this.Wishlist.find();
        } catch (error) {
            Bootstrap.logger.log("error","Error in WishlistRepository.getAllWishlist " + error);
            throw new Error();
        }  
    }

    public async saveOrCreateWishlist(wishlist: IWishlist): Promise<IWishlist> {
        try {            
            return await wishlist.save();
        } catch (error) {
            Bootstrap.logger.log("error","Error in WishlistRepository.saveOrCreateWishlist " + error);
            throw new Error();
        }  
    }

    public async getWishlistById(id: string): Promise<IWishlist> {
        try {
            return await this.Wishlist.findById(id);
        } catch (error) {
            Bootstrap.logger.log("error","Error in WishlistRepository.getWishlistById " + error);
            throw new Error();
        }  
    }

    public async deleteWishlistById(id: string): Promise<void> {
        try {
            return await this.Wishlist.deleteOne({_id: id});
        } catch (error) {
            Bootstrap.logger.log("error","Error in WishlistRepository.deleteWishlistById " + error);
            throw new Error();
        }  
    }
}