import { IWishlist } from '../../interfaces/IWishlist';

export interface IWishlistRepository {
    getAllWishlist(): Promise<IWishlist[]>;
    saveOrCreateWishlist(wishlist: IWishlist): Promise<IWishlist>;
    getWishlistById(id: string): Promise<IWishlist>;
    deleteWishlistById(id: string): Promise<void>;
}