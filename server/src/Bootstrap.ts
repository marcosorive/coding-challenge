import { Logger } from 'winston';
import { Router } from 'express';
import { getProductRoutes } from './routes/ProductRoutes';
import { getWishlistRoutes } from './routes/WishlistRoutes';
import { ProductService } from "./services/ProductService";
import { WishlistService } from "./services/WishlistService";
import { ProductRepository } from './repositories/product/ProductRepository';
import { WishlistRepository } from './repositories/wishlist/WishlistRepository';
import { Product } from './models/Product';
import { Wishlist } from './models/Wishlist';
import { logger } from './utils/logger';

export class Bootstrap{
    public static productRoutes: Router;
    public static productService: ProductService;
    public static wishlistRoutes: Router;
    public static wishlistService: WishlistService;
    public static logger: Logger;

    constructor(){}

    public setup(): void{
        const productRepository = new ProductRepository(Product);
        const productService = new ProductService(productRepository);
        const wishlistRepository = new WishlistRepository(Wishlist);
        const wishlistService = new WishlistService(wishlistRepository, productRepository);

        const productRoutes = getProductRoutes();
        const wishlistRoutes = getWishlistRoutes();

        Bootstrap.productRoutes = productRoutes;
        Bootstrap.productService = productService;
        Bootstrap.wishlistRoutes = wishlistRoutes;
        Bootstrap.wishlistService = wishlistService;
        Bootstrap.logger = logger;
    }
}